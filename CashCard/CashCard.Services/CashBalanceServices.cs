﻿using System;
using System.Data;
using System.Threading;
using CashCard.Model;
using CashCard.Model.Enums;

namespace CashCard.Services
{
    public class CashBalanceServices : ICashBalanceServices
    {
        private readonly IAccountTransaction _accountTransaction;
        private readonly SemaphoreSlim _locker = new SemaphoreSlim(1);

        public CashBalanceServices(IAccountTransaction accountTransaction)
        {
            _accountTransaction = accountTransaction;
        }

        /// <summary>
        /// 1. Ensure that the cash card has been authenticated
        /// 2. Get the latest balance for the account
        /// 3. Calculate new balance after deposit or withdrawal
        /// 4. Update the balance of the account
        /// </summary>
        public async void AdjustBalance()
        {
            await _locker.WaitAsync();
            try
            {
                if (!_accountTransaction.IsAuthenticated)
                    throw new UnauthorizedAccessException("Cash card has not been authenticated");

                var balance = await _accountTransaction.CashCard.GetBalance();

                switch (_accountTransaction.AccountTransactionType)
                {
                    case AccountTransactionType.Deposit:
                        await _accountTransaction.CashCard.UpdateBalance(BalanceAfterDeposit(balance));
                        break;

                    case AccountTransactionType.Withdrawal:
                        await _accountTransaction.CashCard.UpdateBalance(BalanceAfterWithdrawal(balance));
                        break;

                    case AccountTransactionType.None:
                        throw new ArgumentOutOfRangeException();
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            finally
            {
                _locker.Release();
            }
        }

        private decimal BalanceAfterWithdrawal(decimal balance)
        {
            if (_accountTransaction.Amount > balance)
            {
                throw new ArgumentException($"Unable to withdraw amount from cash card:{_accountTransaction.CashCard.AccountId} as insufficient funds. " +
                                            $"Current balance:{ApplyMoneyFormatter(balance)}, requested amount:{ApplyMoneyFormatter(_accountTransaction.Amount)}");
            }

            balance = balance - _accountTransaction.Amount;
            return balance;
        }

        private decimal BalanceAfterDeposit(decimal balance)
        {
            balance += _accountTransaction.Amount;
            return balance;
        }

        private string ApplyMoneyFormatter(decimal value)
        {
            return $"{value:#.00} {_accountTransaction.CashCard.Currency}";
        }
    }
}