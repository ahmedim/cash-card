﻿namespace CashCard.Services
{
    public interface IAuthenticateServices
    {
        bool Authenticate(int pin);
    }
}