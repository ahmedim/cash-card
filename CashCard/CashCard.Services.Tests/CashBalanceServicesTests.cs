﻿using System;
using CashCard.Model;
using NUnit.Framework;

namespace CashCard.Services.Tests
{
    public class CashCardBalanceServiceTests
    {
        private ICashBalanceServices _cashBalanceServices;
        private IAccountTransaction _accountTransaction;

        public CashCardBalanceServiceTests()
        {
            _cashBalanceServices = new CashBalanceServices(_accountTransaction);
        }

        public class When_I_Adjust_Balance : CashCardBalanceServiceTests
        {
            [Test]
            public void It_Should_Fail_If_Card_Is_Not_Authenticated()
            {
                // Given

                // When

                // Then
                Assert.Throws<UnauthorizedAccessException>(() => _cashBalanceServices.AdjustBalance());
            }

            [Test]
            public void It_Should_Fail_If_Transaction_Type_Is_Not_Supported()
            {
                // Given

                // When

                // Then
                Assert.Throws<UnauthorizedAccessException>(() => _cashBalanceServices.AdjustBalance());
            }

            public class When_I_Request_A_Withdrawal : When_I_Adjust_Balance
            {
                [Test]
                public void It_Should_Fail_If_Amount_Requested_Is_Greater_Than_Account_Balance()
                {
                    // Given

                    // When

                    // Then
                    Assert.Throws<ArgumentException>(() => _cashBalanceServices.AdjustBalance());
                }

                [Test]
                public void It_Should_Allow_Withdrawal_If_Amount_Requested_Is_Equal_To_Account_Balance()
                {
                }

                [Test]
                public void It_Should_Allow_Withdrawal_If_Amount_Requested_Is_Less_Than_Account_Balance()
                {
                }
            }

            public class When_I_Request_A_Deposit : When_I_Adjust_Balance
            {
                [Test]
                public void It_Should_Deposit_Money_Into_Account()
                {
                }
            }
        }
    }
}