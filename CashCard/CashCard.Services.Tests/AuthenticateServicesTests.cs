﻿using NUnit.Framework;

namespace CashCard.Services.Tests
{
    public class AuthenticateServicesTests
    {
        public class When_I_Authenticate_A_Cash_Card : AuthenticateServicesTests
        {
            [Test]
            public void It_Should_Fail_Validation_If_Pin_Is_Not_Supplied()
            {
                // Given

                // When

                // Then
            }

            [Test]
            public void It_Should_Fail_Validation_If_Pin_Is_Less_Than_Four_Numerical_Values()
            {
            }

            [Test]
            public void It_Should_Fail_Validation_If_Pin_Is_More_Than_Four_Numerical_Values()
            {
            }

            [Test]
            public void It_Should_Fail_Validation_If_Pin_Contains_Any_Non_Numerical_Values()
            {
            }

            [Test]
            public void It_Should_Fail_Validation_If_Pin_Does_Not_Belong_To_Account()
            {
            }

            [Test]
            public void It_Should_Lock_Account_If_Validation_Fails_More_Than_Accepted_Attempts()
            {
            }

            [Test]
            public void It_Should_Pass_Validation_If_Pin_Belongs_To_Account()
            {
            }
        }
    }
}