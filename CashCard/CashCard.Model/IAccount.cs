﻿using System.Threading.Tasks;

namespace CashCard.Model
{
    public interface IAccount
    {
        long AccountId { get; set; }

        string HashedPin { get; set; }

        string Currency { get; set; }

        Task UpdateBalance(decimal balance);

        Task<decimal> GetBalance();
    }
}