﻿namespace CashCard.Model.Enums
{
    public enum AccountTransactionType
    {
        None = 0,

        Deposit = 1,

        Withdrawal = 2
    }
}
