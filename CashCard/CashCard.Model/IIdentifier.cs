﻿namespace CashCard.Model
{
    public interface IIdentifier
    {
        long Id { get; set; }
    }
}