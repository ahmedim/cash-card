﻿using CashCard.Model.Enums;

namespace CashCard.Model
{
    public interface IAccountTransaction
    {
        ICashCard CashCard { get; set; }

        AccountTransactionType AccountTransactionType { get; set; }

        bool IsAuthenticated { get; set; }

        decimal Amount { get; set; }
    }
}
